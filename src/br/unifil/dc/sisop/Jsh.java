package br.unifil.dc.sisop;


import java.io.*;
import java.util.Scanner;

/**
 * Write a description of class Jsh here.
 *
 * @author Ricardo Inacio Alvares e Silva
 * @version 180823
 */
public final class Jsh {

    public static String comando2;
    public static String variavel = "";
    public static String diretório = System.getProperty("user.dir")+variavel;
    /**
     * Funcao principal do Jsh.
     */
    public static void promptTerminal() {

        while (true) {
            exibirPrompt();
            ComandoPrompt comandoEntrado = lerComando();
            executarComando(comandoEntrado);
        }
    }

    /**
     * Escreve o prompt na saida padrao para o usuário reconhecê-lo e saber que o
     * terminal está pronto para receber o próximo comando como entrada.
     */
    public static void exibirPrompt() {
        System.out.println("---------------------------------------------------------------------------------------------------------");
        String usuario = System.getProperty("user.name");
        System.out.println("Microsoft Windows [versão 10.0.18363.1082]");
        System.out.println("(c) 2019 Microsoft Corporation. Todos os direitos reservados.");
        diretório = System.getProperty("user.dir")+variavel;
        System.out.print(usuario+"#"+diretório+"\\");
    }

    /**
    * Preenche as strings comando e parametros com a entrada do usuario do terminal.
    * A primeira palavra digitada eh sempre o nome do comando desejado. Quaisquer
    * outras palavras subsequentes sao parametros para o comando. A palavras sao
    * separadas pelo caractere de espaco ' '. A leitura de um comando eh feita ate
    * que o usuario pressione a tecla <ENTER>, ou seja, ate que seja lido o caractere
    * EOL (End Of Line).
    *
    * @return 
    */
    public static ComandoPrompt lerComando() {
        Scanner controle = new Scanner(System.in);
        String comando = controle.nextLine();
        comando2 = comando;
        if(comando.indexOf(" ") != -1){
            int index = comando.indexOf(" ");
            comando = comando.substring(0,index);
        }
        ComandoPrompt cp = new ComandoPrompt(comando);
        return cp;
    }


    /**
    * Recebe o comando lido e os parametros, verifica se eh um comando interno e,
    * se for, o executa.
    * 
    * Se nao for, verifica se é o nome de um programa terceiro localizado no atual 
    * diretorio de trabalho. Se for, cria um novo processo e o executa. Enquanto
    * esse processo executa, o processo do uniterm deve permanecer em espera.
    *
    * Se nao for nenhuma das situacoes anteriores, exibe uma mensagem de comando ou
    * programa desconhecido.
    */
    public static void executarComando(ComandoPrompt comando) {
        if(comando.getNome().equals("encerrar")){
            System.exit(0);
        }
        else if(comando.getNome().equals("relogio")){
            System.out.println(ComandosInternos.exibirRelogio());
        }
        else if(comando.getNome().equals("la")){
            ComandosInternos.escreverListaArquivos(diretório);
        }
        else if(comando.getNome().equals("cd")){
            ComandosInternos.criarNovoDiretorio(comando2);
        }
        else if(comando.getNome().equals("ad")){
            ComandosInternos.apagarDiretorio(comando2);
        }
        else if(comando.getNome().equals("mdt")){
            ComandosInternos.mudarDiretorioTrabalho(comando2);
        }
        else if(comando.getNome().equals("mesg_do_dia")){
            msg();
        }
        else if(comando.getNome().equals("falha_arbitraria")){
            arbitraria();
        }
        else{
            File aa = new File(diretório+"\\"+comando.getNome());
            if(aa.exists()){
                executarPrograma(comando);
            }
            else{
                System.out.println("Erro.");
            }
        }
    }

    public static int msg(){
        System.out.println("The only way around is through.");
        return 0;
    }

    public static int arbitraria(){
        System.out.println("Invalid arguments. Please, RTFM.");
        return 1;
    }


    public static int executarPrograma(ComandoPrompt comando) {
        try{
            java.awt.Desktop.getDesktop().open( new File( diretório+"\\"+comando.getNome()) );
        }
        catch(IllegalArgumentException | IOException e1){

            System.out.println(e1);
        }
        return 0;
    }
    
    
    /**
     * Entrada do programa. Provavelmente você não precisará modificar esse método.
     */
    public static void main(String[] args) {

        promptTerminal();
    }
    
    
    /**
     * Essa classe não deve ser instanciada.
     */
    private Jsh() {}
}
