package br.unifil.dc.sisop;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * Write a description of class ComandosInternos here.
 *
 * @author Ricardo Inacio Alvares e Silva
 * @version 180823
 */
public final class ComandosInternos {

    public static String exibirRelogio() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy, 'ás' HH:mm");
        Date date = new Date();
        return "Data e hora de uso: "+dateFormat.format(date);
    }

    public static int escreverListaArquivos(String nomeDir) {
        File dir = new File(nomeDir);
        if (dir.isDirectory())
            System.out.println(nomeDir);
            for (File obj : dir.listFiles())
                System.out.println(obj.toString().substring(obj.toString().lastIndexOf("\\")+1));
        return 0;
    }
    
    public static int criarNovoDiretorio(String nomeDir) {

        if(nomeDir.equals("cd")){
            Jsh.variavel = "";
            return 0;
        }

        nomeDir = nomeDir.substring(nomeDir.indexOf(" ")+1);
        if(nomeDir.indexOf(" ") != -1) {
            int index = nomeDir.indexOf(" ");
            nomeDir = nomeDir.substring(0, index);
        }
        File file = new File(System.getProperty("user.dir")+"\\"+nomeDir);
        if (!file.exists()) {
            file.mkdirs();
        }
        Jsh.variavel = "\\"+nomeDir;
        return 0;
    }
    
    public static int apagarDiretorio(String nomeDir) {
        nomeDir = nomeDir.substring(nomeDir.indexOf(" ")+1);
        if(nomeDir.indexOf(" ") != -1) {
            int index = nomeDir.indexOf(" ");
            nomeDir = nomeDir.substring(0, index);
        }
        File file = new File(System.getProperty("user.dir")+"\\"+nomeDir);
        if (file.exists()) {
            file.delete();
        }
        return 0;

    }
    
    public static int mudarDiretorioTrabalho(String nomeDir) {
        nomeDir = nomeDir.substring(nomeDir.indexOf(" ") + 1);
        if (nomeDir.indexOf(" ") != -1) {
            int index = nomeDir.indexOf(" ");
            nomeDir = nomeDir.substring(0, index);
        }
        File file = new File(System.getProperty("user.dir") + "\\" + nomeDir);
        if (file.exists()) {
            Jsh.variavel = "\\"+nomeDir;
        }
        return 0;
    }
    
    /**
     * Essa classe não deve ser instanciada.
     */
    private ComandosInternos() {}
}
